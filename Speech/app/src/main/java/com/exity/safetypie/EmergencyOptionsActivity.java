package com.exity.safetypie;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.exity.safetypie.pages.ActionsFragment;
import com.exity.safetypie.pages.LocationFragment;
import com.exity.safetypie.pages.NonScrollablePager;
import com.exity.safetypie.pages.TabAdapter;
import com.exity.safetypie.pages.contacts.EmergencyContactsFragment;
import com.google.cloud.android.speech.R;

public class EmergencyOptionsActivity extends AppCompatActivity {

    private static final int CAMERA_PIC_REQUEST = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_options);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(null);

        NonScrollablePager pager = (NonScrollablePager) findViewById(R.id.viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new ActionsFragment(), "Actions");
        adapter.addFragment(new EmergencyContactsFragment(), "Contacts");
        adapter.addFragment(new LocationFragment(), "Location");
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager);
        pager.setPagingEnabled(false);

        findViewById(R.id.close_emergency_screen).setOnClickListener(v -> finish());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_emergency_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.streaming_action:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePictureIntent, CAMERA_PIC_REQUEST);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
