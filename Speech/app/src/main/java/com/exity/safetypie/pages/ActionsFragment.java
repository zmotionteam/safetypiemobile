package com.exity.safetypie.pages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.cloud.android.speech.R;

public class ActionsFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_actions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.action_button_instant_message).setOnClickListener(this);
        view.findViewById(R.id.action_button_emergency_contact).setOnClickListener(this);
        view.findViewById(R.id.action_button_emergency_service).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.action_button_instant_message:
                Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:+375299617728"));
                smsIntent.putExtra("sms_body", "Please, help me and contact my family members as soon as possible!!!");
                startActivity(smsIntent);
                break;
            case R.id.action_button_emergency_contact:
                Intent contactIntent = new Intent(Intent.ACTION_DIAL);
                contactIntent.setData(Uri.parse("tel:+4366565233431"));
                startActivity(contactIntent);
                break;
            case R.id.action_button_emergency_service:
                Intent emergencyIntent = new Intent(Intent.ACTION_DIAL);
                emergencyIntent.setData(Uri.parse("tel:1122"));
                startActivity(emergencyIntent);
                break;
        }
    }
}
