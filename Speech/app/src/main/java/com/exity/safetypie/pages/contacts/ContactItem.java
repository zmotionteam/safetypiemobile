package com.exity.safetypie.pages.contacts;

public class ContactItem {

    enum Type {
        TITLE(0),
        CONTACT(1);

        public int itemType;

        Type(int itemType) {
            this.itemType = itemType;
        }
    }

    public String contactName;

    public int contactImageId;

    public Type type;

    public ContactItem(String contactName, int contactImageId, Type type) {
        this.contactName = contactName;
        this.contactImageId = contactImageId;
        this.type = type;
    }
}
