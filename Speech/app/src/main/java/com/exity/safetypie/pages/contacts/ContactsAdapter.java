package com.exity.safetypie.pages.contacts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.cloud.android.speech.R;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ContactItem> items;

    public ContactsAdapter(List<ContactItem> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ContactItem.Type.CONTACT.itemType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
            return new ContactViewHolder(view);
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_title, parent, false);
        return new TitleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ContactItem.Type.CONTACT.itemType) {
            final ImageView imageView = (ImageView) holder.itemView.findViewById(android.R.id.icon);

            Glide.with(holder.itemView.getContext())
                    .load(items.get(position).contactImageId)
                    .apply(RequestOptions.circleCropTransform())
                    .into(imageView);
        }

        TextView text = (TextView) holder.itemView.findViewById(android.R.id.text1);
        text.setText(items.get(position).contactName);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).type.itemType;
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder {

        public ContactViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class TitleViewHolder extends RecyclerView.ViewHolder {

        public TitleViewHolder(View itemView) {
            super(itemView);
        }
    }
}
