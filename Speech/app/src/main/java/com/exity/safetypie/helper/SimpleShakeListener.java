package com.exity.safetypie.helper;

import android.hardware.SensorListener;
import android.hardware.SensorManager;

public class SimpleShakeListener implements SensorListener {

    public interface ShakeCallback {

        void onPhoneShaked();
    }

    private static int SHAKE_THRESHOLD = 10000;

    private ShakeCallback callback;

    private long lastUpdate = 0;
    private float last_x = 0;
    private float last_y = 0;
    private float last_z = 0;

    public SimpleShakeListener(ShakeCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onSensorChanged(int sensor, float[] values) {
        if (sensor == SensorManager.SENSOR_ACCELEROMETER) {
            long curTime = System.currentTimeMillis();
            // only allow one update every 100ms.
            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float x = values[SensorManager.DATA_X];
                float y = values[SensorManager.DATA_Y];
                float z = values[SensorManager.DATA_Z];

                float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    callback.onPhoneShaked();
                }
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(int i, int i1) {

    }
}
