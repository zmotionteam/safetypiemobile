package com.exity.safetypie.pages.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.cloud.android.speech.R;

import java.util.ArrayList;
import java.util.List;

public class EmergencyContactsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_emergency_contacts, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recycler = (RecyclerView) view.findViewById(R.id.recycler);
        recycler.setAdapter(new ContactsAdapter(getContacts()));
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private List<ContactItem> getContacts() {
        List<ContactItem> contact = new ArrayList<>();

        contact.add(new ContactItem("Emergency contacts", 0, ContactItem.Type.TITLE));

        contact.add(new ContactItem("Artsiom", R.raw.contact_0, ContactItem.Type.CONTACT));
        contact.add(new ContactItem("Yuelin", R.raw.contact_1, ContactItem.Type.CONTACT));

        contact.add(new ContactItem("SafetyPie members close to you", 0, ContactItem.Type.TITLE));

        contact.add(new ContactItem("Norhan", R.raw.contact_2, ContactItem.Type.CONTACT));
        contact.add(new ContactItem("Slava", R.raw.contact_3, ContactItem.Type.CONTACT));
        contact.add(new ContactItem("Tairon", R.raw.contact_4, ContactItem.Type.CONTACT));

        return contact;
    }
}
