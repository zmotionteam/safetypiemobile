package com.exity.safetypie.pages;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.exity.safetypie.App;
import com.google.cloud.android.speech.R;
import com.tomtom.online.sdk.common.location.LatLng;
import com.tomtom.online.sdk.location.LocationUpdateListener;
import com.tomtom.online.sdk.map.Icon;
import com.tomtom.online.sdk.map.MapConstants;
import com.tomtom.online.sdk.map.MapFragment;
import com.tomtom.online.sdk.map.OnMapReadyCallback;
import com.tomtom.online.sdk.map.RouteBuilder;
import com.tomtom.online.sdk.map.RouteStyleBuilder;
import com.tomtom.online.sdk.map.TomtomMap;
import com.tomtom.online.sdk.routing.OnlineRoutingApi;
import com.tomtom.online.sdk.routing.RoutingApi;
import com.tomtom.online.sdk.routing.data.FullRoute;
import com.tomtom.online.sdk.routing.data.RouteQuery;
import com.tomtom.online.sdk.routing.data.RouteQueryBuilder;
import com.tomtom.online.sdk.routing.data.RouteResponse;
import com.tomtom.online.sdk.routing.data.RouteType;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LocationFragment extends Fragment implements OnMapReadyCallback, LocationUpdateListener {

    private TomtomMap tomtomMap;

    private LatLng policeBerlin = new LatLng(52.523430, 13.411440);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_location, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTomTomServices();

        view.findViewById(R.id.police_route_button).setOnClickListener(v -> {
            view.findViewById(R.id.direcion_container).setVisibility(View.GONE);
            planRoute();
        });
    }

    @Override
    public void onMapReady(@NonNull TomtomMap tomtomMap) {
        this.tomtomMap = tomtomMap;
        this.tomtomMap.setMyLocationEnabled(true);
        this.tomtomMap.getMarkerSettings().setMarkersClustering(true);
        this.tomtomMap.setMyLocationEnabled(true);
        this.tomtomMap.getLocationSource().addLocationUpdateListener(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        this.tomtomMap.centerOn(
                this.tomtomMap.getUserLocation().getLatitude(),
                this.tomtomMap.getUserLocation().getLongitude(),
                10,
                MapConstants.ORIENTATION_NORTH);
    }

    private void initTomTomServices() {
        MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getAsyncMap(this);
    }

    private void planRoute() {
        LatLng currentPosition = new LatLng(
                this.tomtomMap.getUserLocation().getLatitude(),
                this.tomtomMap.getUserLocation().getLongitude()
        );

        RoutingApi routingApi = OnlineRoutingApi.create(App.getAppContext());
        RouteQuery routeQuery = new RouteQueryBuilder(currentPosition, policeBerlin).withRouteType(RouteType.FASTEST).build();
        routingApi.planRoute(routeQuery)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<RouteResponse>() {
                    @Override
                    public void onSuccess(RouteResponse routeResponse) {
                        for (FullRoute fullRoute : routeResponse.getRoutes()) {
                            RouteBuilder routeBuilder = new RouteBuilder(fullRoute.getCoordinates());
                            routeBuilder.endIcon(Icon.Factory.fromResources(getActivity(), R.drawable.baseline_star_black_48, 4));
                            routeBuilder.startIcon(Icon.Factory.fromResources(getActivity(), R.drawable.user, 1.5));
                            routeBuilder.style(RouteStyleBuilder.create()
                                    .withWidth(.5)
                                    .withFillColor(Color.BLACK)
                                    .build());

                            LocationFragment.this.tomtomMap.addRoute(routeBuilder);
                            LocationFragment.this.tomtomMap.centerOn(
                                    LocationFragment.this.tomtomMap.getUserLocation().getLatitude(),
                                    LocationFragment.this.tomtomMap.getUserLocation().getLongitude(),
                                    12,
                                    MapConstants.ORIENTATION_NORTH);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }
}
