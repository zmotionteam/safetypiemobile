package com.exity.safetypie.main;

import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.exity.safetypie.App;
import com.google.cloud.android.speech.R;
import com.tomtom.online.sdk.common.location.LatLng;
import com.tomtom.online.sdk.location.LocationUpdateListener;
import com.tomtom.online.sdk.map.Icon;
import com.tomtom.online.sdk.map.MapConstants;
import com.tomtom.online.sdk.map.MapFragment;
import com.tomtom.online.sdk.map.MarkerBuilder;
import com.tomtom.online.sdk.map.OnMapReadyCallback;
import com.tomtom.online.sdk.map.RouteBuilder;
import com.tomtom.online.sdk.map.RouteStyleBuilder;
import com.tomtom.online.sdk.map.SimpleMarkerBalloon;
import com.tomtom.online.sdk.map.TomtomMap;
import com.tomtom.online.sdk.routing.OnlineRoutingApi;
import com.tomtom.online.sdk.routing.RoutingApi;
import com.tomtom.online.sdk.routing.data.FullRoute;
import com.tomtom.online.sdk.routing.data.RouteQuery;
import com.tomtom.online.sdk.routing.data.RouteQueryBuilder;
import com.tomtom.online.sdk.routing.data.RouteResponse;
import com.tomtom.online.sdk.routing.data.RouteType;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainMapFragment extends Fragment implements OnMapReadyCallback, LocationUpdateListener {

    private TomtomMap tomtomMap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTomTomServices();
    }

    @Override
    public void onMapReady(@NonNull TomtomMap tomtomMap) {
        this.tomtomMap = tomtomMap;
        this.tomtomMap.setMyLocationEnabled(true);
        this.tomtomMap.getLocationSource().addLocationUpdateListener(this);
        setMarkers();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.tomtomMap.centerOn(
                this.tomtomMap.getUserLocation().getLatitude(),
                this.tomtomMap.getUserLocation().getLongitude(),
                10,
                MapConstants.ORIENTATION_NORTH);
    }

    private void setMarkers() {
        List<LatLng> dangerZones = new ArrayList<>();
        dangerZones.add(new LatLng(52.5215329, 13.4037969));
        dangerZones.add(new LatLng(52.5120227, 13.3843008));
        dangerZones.add(new LatLng(52.4953445, 13.4132657));
        dangerZones.add(new LatLng(52.4787504, 13.3656982));

        dangerZones.add(new LatLng(52.4994593, 13.2773695));
        dangerZones.add(new LatLng(52.5204528, 13.3105733));
        dangerZones.add(new LatLng(52.5120174, 13.370856));

        for (LatLng position : dangerZones) {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            String markerTitle = "Danger area in Berlin";
            try {
                addresses = geocoder.getFromLocation(position.getLatitude(), position.getLongitude(), 1);
                String address = addresses.get(0).getAddressLine(0);
                markerTitle = String.format("Danger area at\n%s", address.split(",")[0]);
            } catch (Exception e) {

            }

            MarkerBuilder markerBuilder = new MarkerBuilder(position)
                    .icon(Icon.Factory.fromResources(getActivity(), R.drawable.dangerous_region))
                    .markerBalloon(new SimpleMarkerBalloon(markerTitle));
            this.tomtomMap.addMarker(markerBuilder);

        }
    }

    private void initTomTomServices() {
        MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getAsyncMap(this);
    }

    public void planRoute(LatLng destination) {
        LatLng currentPosition = new LatLng(
                this.tomtomMap.getUserLocation().getLatitude(),
                this.tomtomMap.getUserLocation().getLongitude()
        );

        RoutingApi routingApi = OnlineRoutingApi.create(App.getAppContext());
        RouteQuery routeQuery = new RouteQueryBuilder(currentPosition, destination).withRouteType(RouteType.FASTEST).build();
        routingApi.planRoute(routeQuery)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<RouteResponse>() {
                    @Override
                    public void onSuccess(RouteResponse routeResponse) {
                        for (FullRoute fullRoute : routeResponse.getRoutes()) {
                            RouteBuilder routeBuilder = new RouteBuilder(fullRoute.getCoordinates());
                            routeBuilder.endIcon(Icon.Factory.fromResources(getActivity(), R.drawable.baseline_star_black_48, 4));
                            routeBuilder.startIcon(Icon.Factory.fromResources(getActivity(), R.drawable.user_main_map, 1.5));
                            routeBuilder.style(RouteStyleBuilder.create()
                                    .withWidth(.5)
                                    .withFillColor(Color.BLACK)
                                    .build());

                            MainMapFragment.this.tomtomMap.addRoute(routeBuilder);
                            MainMapFragment.this.tomtomMap.centerOn(
                                    MainMapFragment.this.tomtomMap.getUserLocation().getLatitude(),
                                    MainMapFragment.this.tomtomMap.getUserLocation().getLongitude(),
                                    12,
                                    MapConstants.ORIENTATION_NORTH);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    public LatLng getCurrentPosition() {
        return new LatLng(
                this.tomtomMap.getUserLocation().getLatitude(),
                this.tomtomMap.getUserLocation().getLongitude()
        );
    }
}
