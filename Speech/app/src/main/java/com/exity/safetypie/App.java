package com.exity.safetypie;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static Context APP_CONTEXT;

    @Override
    public void onCreate() {
        super.onCreate();
        APP_CONTEXT = getApplicationContext();
    }

    public static Context getAppContext() {
        return APP_CONTEXT;
    }
}
